# Deposit Account Proof of Concept

This is a proof of concept for deploying accounts for accepting deposits of ERC20 tokens as well as a polling mechanism for detecting new payments.

Detection of payments may be useful for use cases such as updating a user's balance in the database or moving funds from a user's deposit account to a centralized wallet.


## Setup

- Clone this repository,
- Install the package dependencies `npm i`.

## Configuration

Before running the server, you will need to configure the environment variables.

Copy the .env.example to .env:

```
cp .env.example .env
```

Change the MNEMONIC value to a value of your choice.

## Running a local blockchain node

You will need to run a local copy of the Ethereum blockchain and deploy the Accounts contracts before running the deposit server.

Clone the Accounts repo: 

```
git clone git@gitlab.com:fidira/exchange/accounts.git
```

Compile and deploy the contracts to a running local node:

```
npx hardhat node
```

## Running the server

- Run the server:

```
./node_modules/nodemon/bin/nodemon.js --experimental-json-modules index.js
```

## Testing

The process for depositing an account is:

- Deploy a new deposit account contract via the Account Factory contract,
- Deposit funds to the computed deposit account address.

The account's address is predetermined by using a UUID from the database that is uniquely associated with the user.  

### Deploy a deposit account

Assuming that the deposit server is deployed to localhost 3000, call the /accounts endpoint to deploy a new Account contract:

```
curl -X POST -d "{id: some_uuid}" http://localhost:3000/accounts
```

where some_uuid is a unique identifier (ideally from a database's primary key field).

If the account contract deployment succeeds, a 201 Created should be returned. If not (for example, the contract is already deployed), you should receive a  generic 404 error.

### Deposit funds to the account

The newly created account will be output to the console. Copy this value and use Metamask or some other wallet to transfer funds to the deposit account. Make sure your wallet is connected to the Hardhat localhost node or funds may be lost forever.

Once successfully detected, the polling server should output the ERC20 token contract address and the amount of tokens transferred (as an integer). It is this console.log that should be changed for some function that actually does something with the funds.