import "dotenv/config";

export const mnemonic = process.env.MNEMONIC || "";

export const networkConfig = {
  localhost: {
    contracts: {
      accountFactory: "0xDe252713512db4B6f67547DE6A3A69b8F233688E",
      withdrawalProxy: "0x487FaBac29906B0E244911A66C745F0c0e79E269"
    },
    provider: "http://127.0.0.1:8545/"
  }
};
