import ethers from "ethers";
import express from 'express';
import poll from "./utils/deposits/poll.js";
import computeAccountAddress from "./utils/computeAccountAddress.js";
import getAccountSalt from "./utils/getAccountSalt.js";
import artifactAccountFactory from "./constants/contracts/AccountFactory.json";
import { networkConfig } from "./config.js";

// Poll for deposits.
// store this to db or file.
global.LAST_FROM_BLOCK = 1;

async function main() {
  while (true) {
    await poll();
    
    await new Promise((r) => setTimeout(r, 30000));
  }
}

main();

// launch an depoosit account manager.
const app = express();
app.use(express.json());

app.get("/", (req, res) => {
  res.send('Hello World!');
});

app.post("/accounts", async (req, res) => {
  const provider = new ethers.providers.JsonRpcProvider(networkConfig.localhost.provider);
  console.log("uuid", req.body.id);
  const computedAccountAddress = computeAccountAddress(req.body.id);
  
  const accountFactory = new ethers.Contract(networkConfig.localhost.contracts.accountFactory, artifactAccountFactory.abi, provider.getSigner(0));
  
  if (await provider.getCode(computedAccountAddress) === "0x") {
    console.log("create account", computedAccountAddress);
    accountFactory.deployAccount(getAccountSalt(req.body.id), networkConfig.localhost.contracts.withdrawalProxy);
    res.sendStatus(201);
  } else {
    res.sendStatus(404);
  }
});

app.listen(3000, () =>
  console.log('Fidira deposit accounts app listening on port 3000!'),
);

