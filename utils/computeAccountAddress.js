import ethers from "ethers";
import getAccountSalt from "./getAccountSalt.js";
import artifactAccount from "../constants/contracts/Account.json";
import { networkConfig } from "../config.js";

export default (uuid) => {
  return ethers.utils.getCreate2Address(
    networkConfig.localhost.contracts.accountFactory,
    getAccountSalt(uuid),
    ethers.utils.keccak256(artifactAccount.bytecode)
  );
};