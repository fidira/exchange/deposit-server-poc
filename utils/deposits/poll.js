import ethers from "ethers";
import computeAccountAddress from "../computeAccountAddress.js";

export default async () => {
    const deposits = [];
    
    // load this from db.
    const users = [
      {uuid: 1},
      {uuid: 2}
    ];
    
    const provider = new ethers.providers.JsonRpcProvider("http://127.0.0.1:8545/");

    // the the getAccountAddress gets the account contract's address for each user. This runs fine for 1K users. May need to split for 10 or 100K+.
    users.forEach((user) => {
      deposits.push(ethers.utils.hexZeroPad(computeAccountAddress(user.uuid), 32));
    });
    
    const fromBlock = LAST_FROM_BLOCK;
    const toBlock = "latest";
    
    console.log("filtering from", fromBlock, "to", toBlock);

    const filter = {
      fromBlock: fromBlock,
      toBlock: toBlock,
      topics: [
        ethers.utils.id("Transfer(address,address,uint256)"),
        null,
        deposits
      ]
    };

    let abiEventTransfer = [
    "event Transfer(address indexed from, address indexed to, uint256 value)"];

    let iface = new ethers.utils.Interface(abiEventTransfer);
    const logs = await provider.getLogs(filter);

    logs.forEach((log) => {
        // do something with the Transfer event's "value". args has the to and from also.
        console.log("deposit", "token", log.address, "amount", iface.parseLog(log).args.value.toString());
    });

    // Store the latest block if successful and feed back into polling filter on next cycle.
    LAST_FROM_BLOCK = (await provider.getBlock("latest")).number;    
};
