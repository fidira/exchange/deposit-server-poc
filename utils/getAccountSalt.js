import ethers from "ethers";
import { networkConfig } from "../config.js";

export default (uuid) => {
  // db.user.uuid is some kind of unique primary key in the database. This value should be retrievable always and should never change for a user (immutable).
  return ethers.utils.solidityKeccak256(
  ["address", "string"],
  [networkConfig.localhost.contracts.accountFactory, uuid]);
};
